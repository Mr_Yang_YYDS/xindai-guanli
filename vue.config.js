const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  configureWebpack: {
    // 开发服务器
    devServer: {
      // 设置一个代理服务器
      proxy: {
        '/api': {
          // 目标元
          target: 'http://124.223.69.156:6300',
          //开启
          changeOrigin: true,
          // 后台没有api这个前缀，所以需要精选一个替换，用 / 代替 api
          pathRewrite: { '^/api': '/' }
        }
      }
    }
  }
})
