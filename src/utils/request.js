import axios from 'axios'
import router from '@/router';
import { Notification, Message } from 'element-ui';
// import { config } from 'vue/types/umd';

// 创建一个axios实例
const requset = axios.create(
    {
        // 添加api前缀
        baseURL: '/api',
        // 请求的过期时间
        timeout: 5000,
    }
)

// 请求拦截器
requset.interceptors.request.use(config => {
    // 给headers添加token
    // console.log('------------------------111--');
    //获取token
    let token = localStorage.getItem('token')
    if(token){
        // 如果token有值的话就存到headers.token里面,赋值给headers
        config.headers.token = token
    }
    return config
})

// 响应拦截器
requset.interceptors.response.use(
    response => {
        // 后端状态码20000表示成功
        if (response?.data?.code === 20000) {
            //后端返回的结构，没有统一，兼容处理一下
            // 判断response下的data.data
            if (typeof response?.data?.data === 'string')
                // 进行返回
                Message.success(response?.data?.data)
            if (typeof response?.data?.data?.info === 'string')
                Message.success(response?.data?.data?.info)
            //返回前端
            return response
        } else if (response?.data?.code === 603) {
            // 603表示token失效
            Notification.error({
                title: '错误',
                message: 'token失效,请重新登陆'
            })
            // 替换到登陆页面
            // 先获取当前访问的url,然后做一个分割
            let url = window.location.href.split('/')
            // 当前访问的页面不是login页面,跳转到登陆页面
            if (url[url.length - 1] !== 'login') {
                router.replace('/login')
            }
        }else{
            // 原始状态下也做一个校验
            if(response?.status !== 200){
                Notification.error({
                    title:'错误',
                    message:'响应错误'
                })
            }
        }
        return response
    }
)

export default requset