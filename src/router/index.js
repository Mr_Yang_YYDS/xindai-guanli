import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginView from '../views/LoginView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect:'/login',
    component: LoginView
  },
  {
    path: '/login',
    name: 'login',
    component: () => import( '../views/LoginView.vue')
  },
  {
    path: '/',
    redirect:'/home',
    component: () => import( '../layout/layout.vue'),
    children:[
      {
        meta:{
          title:'首页'
        },
        path: '/home',
        name: 'home',
        component: () => import( '../views/HomeView.vue')
      },
    ]
  },
  {
    path: '/loan',
    redirect:'/loan-input/index',
    component: () => import( '@/layout/layout.vue'),
    children:[
      {
        meta:{
          title:'贷款申请'
        },
        path: '/loan-input/index',
        name: 'loan-input',
        component: () => import( '@/views/loan/IndexView.vue')
      },
    ]
  },
  {
    path: '/application-manage',
    // redirect:'/application-manage/index',
    meta:{
      title:'申请管理'
    },
    component: () => import( '@/layout/layout.vue'),
    children:[
      {
        meta:{
          title:'申请列表'
        },
        path: '/application-manage/index',
        name: 'application-manage',
        component: () => import( '@/views/application-manage/IndexView.vue')
      },
    ]
  },
  {
    path: '/loan-approval',
    meta:{
      title:'贷款审批'
    },
    component: () => import( '@/layout/layout.vue'),
    children:[
      {
        meta:{
          title:'初审'
        },
        path: '/loan-approval/first',
        name: 'trial',
        component: () => import( '@/views/loan-approval//FirstView.vue')
      },
      {
        meta:{
          title:'终审'
        },
        path: '/loan-approval/end',
        name: 'final',
        component: () => import( '@/views/loan-approval//EndView.vue')
      },
    ]
  },
  {
    path: '/contract-manage',
    redirect: '/contract-manage/index',
    meta: {
      title: '合同管理'
    },
    component: () => import('@/layout/layout.vue'),
    children: [
      {
        path: '/contract-manage/index',
        name: 'contract-manage',
        meta: {
          title: '合同列表'
        },
        component: () => import('@/views/contract-manage/IndexView.vue'),
      },
    ]
  },
  {
    path: "/permission",
    component: ()=> import('@/layout/layout.vue'),
    redirect: "/permission/create",
    meta: {
      title: "权限管理"
    },
    children: [
      {
        path: "/permission/create",
        component: ()=> import("@/views/permission/create.vue"),
        name: "create",
         meta: {
          title: "创建管理员"
        }
      },
      {
        path: "/permission/list",
        component: ()=> import("@/views/permission/list.vue"),
        name: "list",
         meta: {
          title: "列表展示"
        }
      }
    ]
  },
  // ------
  // {
  //   path: "/ceshi",
  //   name:'ceshi',
  //   component: ()=> import('@/views/Ceshi/ceshi.vue'),
  //   meta: {
  //     title: "测试"
  //   },
  // },
]

const router = new VueRouter({
  routes,
  // linkActiveClass:'selected'
})

export default router
