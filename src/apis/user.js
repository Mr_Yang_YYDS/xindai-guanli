import requset from "@/utils/request";

export const doLogin = (user) => {
    return requset.post('/user/login', {
        // 参数
        account: user.username,
        password: user.pass,
        // approve123456.
        // 定义完以后去登录页面引入
    })
}

//退出登录
export const logout =()=>{
    return requset.post('/user/logout')
}

//创建用户接口
export const createUser =({username,password,permission})=>{
    return  requset.post('/permission/createUser',{
        account:username,
        password,
        role_id:permission
    }) 
}

//获取用户接口
export const userList =()=>{
    return requset({
        url: '/user/list?type=new',
        method: 'get'
    })
}

// 获取用户角色
// export const userInfo = ()=>{
//     return requset.get("/user/info")
// }