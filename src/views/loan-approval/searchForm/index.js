const option = [
    {
        label: '姓名',
        prop: 'name',
        type:'Input'
    },
    {
        label: '出生日期',
        prop: 'birthday',
        type:'Input',
        disabled:false
    },
]

export const formOption=()=>{
    return option
}