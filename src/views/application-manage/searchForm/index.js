const option = [
    {
        label: '姓名',
        prop: 'name',
        type: "Input"
    },
    {
        label: '性别',
        prop: 'sex',
        type: "Select",
        filter:['男'],
        options: [{
            value: 'man',
            label: '男'
          }, {
            value: 'woman',
            label: '女'
          }]
    },
]

export const formOption=()=>{
    return option
}